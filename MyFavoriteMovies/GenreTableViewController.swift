//
//  GenreTableViewController.swift
//  MyFavoriteMovies
//
//  Created by Tobias Ostner on 18/07/15.
//  Copyright © 2015 Tobias Ostner. All rights reserved.
//

import UIKit

class GenreTableViewController: UITableViewController {
    
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var movies = [Movie]()
    var genreID: Int?
    
    // ******************************
    // MARK: Lifecycle
    // ******************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genreID = getGenreIDFromItemTag(navigationController!.tabBarItem.tag)
        print("Genre id: \(genreID)")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let methodParameters = [
            "api_key": appDelegate.API_KEY
        ]
        
        let method = "/genre/\(genreID!)/movies"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameters)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            let results = parsedData["results"] as! [[String: AnyObject]]
            self.movies = Movie.moviesFromResults(results)
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.tableView.reloadData()
            }
        }
        
        task?.resume()
    }
    
    // ******************************
    // MARK: UITableViewController
    // ******************************
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellReuseIdentifier = "MovieTableViewCell"
        let movie = movies[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath)
        
        cell.textLabel!.text = movie.title
        
        return cell
    }
    
    
    // ******************************
    // MARK: Navigation
    // ******************************
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail = segue.destinationViewController as! MovieDetailViewController
        let indexPath = tableView.indexPathForCell(sender as! UITableViewCell)!
        detail.movie = movies[indexPath.row]
    }


}

extension GenreTableViewController {
    func getGenreIDFromItemTag(itemTag: Int) -> Int {
        let genres = [
            "Sci-Fi",
            "Comedy",
            "Action"
        ]
        
        let genreMap = [
            "Action": 28,
            "Sci-Fi": 878,
            "Comedy": 35
        ]
        
        return genreMap[genres[itemTag]]!
    }
}
