//
//  LoginViewController.swift
//  MyFavoriteMovies
//
//  Created by Tobias Ostner on 16/07/15.
//  Copyright © 2015 Tobias Ostner. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: UIButton!
    
    var tapRecognizer: UITapGestureRecognizer!
    
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // ******************************
    // MARK: Lifecycle
    // ******************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapRecognizer = UITapGestureRecognizer(target: self, action: "tapToDismissKeyboard")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        usernameTextfield.text = "tbsostnr"
        passwordTextfield.text = "ff7s9mcX9COL"
        addKeyBoardDismissRecognizer()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardDismissRecognizer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // ******************************
    // MARK: Actions
    // ******************************
    
    func tapToDismissKeyboard() {
        print("tapped")
        view.endEditing(true)
    }
    
    @IBAction func login() {
        
        view.endEditing(true)
        
        guard let username = usernameTextfield.text where !username.isEmpty,
              let password = passwordTextfield.text where !password.isEmpty else {
                print("authentication information are empty")
                return
        }
        
        toggleLoginIndication()
        getRequestToken()
    }
    
    // ******************************
    // MARK: Get Session ID
    // ******************************
    
    func getRequestToken() {
        let methodParameters = [
            "api_key": appDelegate.API_KEY
        ]
        
        let method = "/authentication/token/new"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameters)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                self.toggleLoginIndication()
                return
            }
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [String: AnyObject]
            if let token = parsedData["request_token"] as? String {
                print("Request Token: \(token)")
                self.appDelegate.requestToken = token
                self.authenticateRequestToken()
            }
            else {
                print(parsedData)
                self.toggleLoginIndication()
            }
        }
        
        task?.resume()
    }
    
    func authenticateRequestToken() {
        let methodParameters = [
            "api_key": appDelegate.API_KEY,
            "username": usernameTextfield.text!,
            "password": passwordTextfield.text!,
            "request_token": appDelegate.requestToken!
        ]
        
        let method = "/authentication/token/validate_with_login"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameters)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                self.toggleLoginIndication()
                return
            }
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [String: AnyObject]
            if let isSuccess = parsedData["success"] as? Bool {
                print("authentication of request token success: \(isSuccess)")
                self.getSessionID()
            }
            else {
                print(parsedData)
                self.toggleLoginIndication()
            }
        }
        
        task?.resume()
    }
    
    func getSessionID() {
        let methodParameter = [
            "api_key": appDelegate.API_KEY,
            "request_token": appDelegate.requestToken!
        ]
        
        let method = "/authentication/session/new"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameter)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                self.toggleLoginIndication()
                return
            }
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [String: AnyObject]
            if let sessionId = parsedData["session_id"] as? String {
                self.appDelegate.sessionID = sessionId
                print("Sesssion ID: \(sessionId)")
                self.getUserID()
            }
            else {
                print(parsedData)
                self.toggleLoginIndication()
            }
        }
        
        task?.resume()
    }

    func getUserID() {
        let methodParameter = [
            "api_key": appDelegate.API_KEY,
            "session_id": appDelegate.sessionID!
        ]
        
        let method = "/account"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameter)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                self.toggleLoginIndication()
                return
            }
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [String: AnyObject]
            if let userId = parsedData["id"] as? Int {
                print("User id: \(userId)")
                self.appDelegate.userID = userId
                self.completeLogin()
            }
            else {
                print(parsedData)
                self.toggleLoginIndication()
            }
        }
        
        task?.resume()
    }
    
    func completeLogin() {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            self.toggleLoginIndication()
            self.performSegueWithIdentifier("ShowMoviesTabBar", sender: nil)
        }
        print("Present Movies Tabbar Controller")
    }
    
    // ******************************
    // MARK: Helper
    // ******************************
    
    func addKeyBoardDismissRecognizer() {
        view.addGestureRecognizer(tapRecognizer)
    }
    
    func removeKeyboardDismissRecognizer() {
        view.removeGestureRecognizer(tapRecognizer)
    }
    
    func toggleLoginIndication() {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            self.loginButton.hidden = !self.loginButton.hidden
            self.spinner.isAnimating() ? self.spinner.stopAnimating() : self.spinner.startAnimating()
        }
    }
    
    
}













