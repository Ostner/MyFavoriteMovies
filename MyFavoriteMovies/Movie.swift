//
//  Movie.swift
//  MyFavoriteMovies
//
//  Created by Tobias Ostner on 18/07/15.
//  Copyright © 2015 Tobias Ostner. All rights reserved.
//

import Foundation

struct Movie {
    
    let title: String
    let id: Int
    let posterPath: String?
    
    init(dictionary: [String: AnyObject]) {
        self.title = dictionary["title"] as! String
        self.id = dictionary["id"] as! Int
        self.posterPath = dictionary["poster_path"] as? String
    }
    
    static func moviesFromResults(results: [[String: AnyObject]]) -> [Movie] {
        var movies = [Movie]()
        for result in results {
            movies += [Movie(dictionary: result)]
        }
        return movies
    }
    
}

func ==(lhs: Movie, rhs: Movie) -> Bool {
    return lhs.title == rhs.title
}
