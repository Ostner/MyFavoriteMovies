//
//  AppDelegate.swift
//  MyFavoriteMovies
//
//  Created by Tobias Ostner on 16/07/15.
//  Copyright © 2015 Tobias Ostner. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /* Constants for TheMovieDB */
    let API_KEY = "05277b856bf9ecdff734957b6ba1b794"
    let BASE_URL_STRING = "http://api.themoviedb.org/3"
    let BASE_URL_SECURE_STRING = "https://api.themoviedb.org/3"
    let BASE_IMAGE_URL_SECURE_STRING = "https://image.tmdb.org/t/p"
    
    /* Need these for login */
    var requestToken: String?
    var sessionID: String?
    var userID: Int?
    
    /* Configuration for TheMovieDB */
    // TODO


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // TODO: update configuration if exceeded.
        return true
    }

}

// MARK: Helper

extension AppDelegate {
    
    func escapedParameters(parameters: [String: AnyObject]) -> String {
        
        var urlVars = [String]()
        
        for (key, value) in parameters {
            let stringValue = "\(value)"
            let escapedValue = stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
            urlVars += [key + "=" + "\(escapedValue!)"]
        }
        
        return (!urlVars.isEmpty ? "?" : "") + "&".join(urlVars)
    }
}

