//
//  FavoritesTableViewController.swift
//  MyFavoriteMovies
//
//  Created by Tobias Ostner on 20/07/15.
//  Copyright © 2015 Tobias Ostner. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController {
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var movies = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let methodParameters = [
            "api_key": appDelegate.API_KEY,
            "session_id": appDelegate.sessionID!
        ]
        
        let method = "/account/\(appDelegate.userID!)/favorite/movies"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameters)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            if let results = parsedData["results"] as? [[String: AnyObject]] {
                print("Downloaded favorites movies")
                self.movies = Movie.moviesFromResults(results)
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.tableView.reloadData()
                }
            }
        }
        
        task?.resume()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellReuseIdentifier = "FavoriteTableViewCell"
        let movie = movies[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath)
        
        cell.textLabel?.text = movie.title
        
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    // ******************************
    // MARK: Navigation
    // ******************************
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail = segue.destinationViewController as! MovieDetailViewController
        let indexPath = tableView.indexPathForCell(sender as! UITableViewCell)!
        detail.movie = movies[indexPath.row]
    }

}
