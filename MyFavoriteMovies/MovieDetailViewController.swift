//
//  MovieDetailViewController.swift
//  MyFavoriteMovies
//
//  Created by Tobias Ostner on 18/07/15.
//  Copyright © 2015 Tobias Ostner. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var favoriteButtton: UIButton!
    @IBOutlet weak var unfavoriteButton: UIButton!
    
    var movie: Movie?
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let movie = movie {
            
            titleLabel.text = movie.title
            unfavoriteButton.hidden = true
            
            setFavoriteButton()
            getPosterArt()
        }
        
    }
    
    @IBAction func unFavorite(sender: UIButton) {
        print("unfavorite movie")
        favoriteMovie(false)
    }
    
    @IBAction func favorite(sender: UIButton) {
        print("favorite movie")
        favoriteMovie(true)
    }
    
    func favoriteMovie(favorite: Bool) {
        let methodParameters = [
            "api_key": appDelegate.API_KEY,
            "session_id": appDelegate.sessionID!
        ]
        
        let method = "/account/\(appDelegate.userID!)/favorite"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameters)
        let url = NSURL(string: urlString)!
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let mediaType = "\"media_type\": \"movie\""
        let mediaID = "\"media_id\": \"\(movie!.id)\""
        let shouldFavorite = "\"favorite\": \(favorite)"
        let body = "{\n \(mediaType), \n \(mediaID), \n \(shouldFavorite) \n }"
        request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)
        
        print("HTTP body: \(body)")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            print("Data: \(parsedData)")
            let status = favorite ? 1 : 13
            if let isSuccess = parsedData["status_code"] where (isSuccess as! Int) == status {
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    if favorite {
                        self.favoriteButtton.hidden = true
                        self.unfavoriteButton.hidden = false
                    }
                    else {
                        self.unfavoriteButton.hidden = true
                        self.favoriteButtton.hidden = false
                    }
                }
            }
        }
        
        task?.resume()
    }
    
    func setFavoriteButton() {
        guard let movie = movie else {
            return
        }
        
        let methodParameters = [
            "api_key": appDelegate.API_KEY,
            "session_id": appDelegate.sessionID!
        ]
        
        let method = "/account/\(appDelegate.userID!)/favorite/movies"
        let urlString = appDelegate.BASE_URL_SECURE_STRING + method + appDelegate.escapedParameters(methodParameters)
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            let parsedData = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            print(parsedData)
            var isFavorite = false
            let results = parsedData["results"] as! [[String: AnyObject]]
            for result in Movie.moviesFromResults(results) {
                if movie == result {
                    isFavorite = true
                }
            }
            NSOperationQueue.mainQueue().addOperationWithBlock {
                if isFavorite {
                    self.favoriteButtton.hidden = true
                    self.unfavoriteButton.hidden = false
                }
                else {
                    self.favoriteButtton.hidden = false
                    self.unfavoriteButton.hidden = true
                }
            }
            
        }
        
        task?.resume()
    }
    
    func getPosterArt() {
        guard let posterPath = movie?.posterPath else {
            return
        }
        
        print("Poster path: \(posterPath)")
        let urlString = appDelegate.BASE_IMAGE_URL_SECURE_STRING + "/original" + posterPath
        let url = NSURL(string: urlString)!
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let image = UIImage(data: data!) {
                print("Set image")
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.posterImageView.image = image
                }
            }
        }
        
        task?.resume()
    }
}
